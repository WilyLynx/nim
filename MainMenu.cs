using System;
using System.Collections.Generic;
using System.Linq;
using nim.Games;
using nim.Players;

namespace nim
{
    class MainMenu
    {
        private List<GameDefinition> games;
        private List<PlayerType> playerTypes;

        public MainMenu(List<GameDefinition> games)
        {
            this.games = games;
            this.playerTypes = new List<PlayerType>
            {
                PlayerType.Human,
                PlayerType.Random,
                PlayerType.RandomHeap,
                PlayerType.HighestHeap,
                PlayerType.Optimal
            };
        }

        public void Run()
        {
            var selectedGame = SelectGame();
            var leftPlayer = ChoosePlayerType(0);
            var rightPlayer = ChoosePlayerType(1);
            var game = new InteractiveGame(selectedGame.ToGameState(), leftPlayer, rightPlayer);
            game.Run();
        }

        private void PrintAvailableGames()
        {
            Console.WriteLine("Available games:");
            int i = 0;
            foreach (var g in games)
            {
                Console.WriteLine($"{i, 2}. {g.ToString()}");
                i++;
            }
            
            Console.WriteLine($"{GetCustomGameIndex(),2}. Custom game");
        }

        private GameDefinition SelectGame()
        {
            PrintAvailableGames();
            int gameIndex = InputReader.ReadNumber("Select game: ", min: 0, max: games.Count);
            return gameIndex == GetCustomGameIndex() ? CreateCustomGame() : games[gameIndex];
        }

        private GameDefinition CreateCustomGame()
        {
            int heapCount = InputReader.ReadNumber("Please select number of heaps", min: 1);
            List<int> heapSizes = Enumerable
                .Range(0, heapCount)
                .Select(i => InputReader.ReadNumber($"Please select high of heap number {i}", min: 1))
                .ToList();
            return new GameDefinition(heapSizes);
        }
        private Player ChoosePlayerType(int playerNumber)
        {            
            PrintAvailablePlayers();
            int index = InputReader.ReadNumber($"Select player {playerNumber} type", max: playerTypes.Count);
            PlayerType playerType = playerTypes[index];

            bool isBlocking = false;
            if (playerType != PlayerType.Human)
            {
                Console.WriteLine("You selected computer, do you prefer non blocking or blocking mode?");
                Console.WriteLine("In blocking mode you will be asked to press any key to continue before every computer move.");
                Console.WriteLine("It may be usefull if you would like to trace computer moves.");
                int mode = InputReader.ReadNumber("Select mode (0 = non blocking, 1 - blocking): ", max: 1);
                isBlocking = mode == 1;
            }

            Console.Write("Player name: ");
            string name = Console.ReadLine();

            switch (playerType)
            {
                case PlayerType.Human:
                    return new HumanPlayer(name);
                case PlayerType.Random:
                    return new RandomPlayer(name, isBlocking);
                case PlayerType.RandomHeap:
                    return new RandomHeapPlayer(name, isBlocking);
                case PlayerType.HighestHeap:
                    return new HighestHeapPlayer(name, isBlocking);
                case PlayerType.Optimal:
                    return new BestPlayer(name,isBlocking);
                default:
                    throw new NotImplementedException();
            }
        }

        private void PrintAvailablePlayers()
        {
            Console.WriteLine();
            Console.WriteLine("Available player types");
            foreach ((int id, PlayerType playerType) in Enumerable.Zip(Enumerable.Range(0, playerTypes.Count), playerTypes))
            {
                Console.WriteLine($"{id, 2}. {playerType}");
            }
        }
        
        private int GetCustomGameIndex()
        {
            return games.Count;
        }
    }
}