using nim.Games;

namespace nim
{
    public class Move
    {
        public int HeapNum { get;}
        public int NumberOfElementsToTake { get; }

        public Move(int heapNum, int numberOfElementsToTake)
        {
            this.HeapNum = heapNum;
            this.NumberOfElementsToTake = numberOfElementsToTake;
        }

        public bool IsValid(GameState state)
        {
            if(HeapNum < 0 || HeapNum >= state.HeapsHeights.Count)
                throw new MoveNotAcceptedException("Heap number out of range.");
            if(NumberOfElementsToTake < 1)
                throw new MoveNotAcceptedException("You have to take at least one element from heap.");
            if(state.HeapsHeights[HeapNum] == 0)
                throw new MoveNotAcceptedException("Selected heap is empty.");
            if(state.HeapsHeights[HeapNum] < NumberOfElementsToTake)
                throw new MoveNotAcceptedException("Heap size is smaller than number of elements in move.");
            return true;            
        }
    }
}