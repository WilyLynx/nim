using System;

namespace nim.Games
{
    class InteractiveGame : Game
    {
        public InteractiveGame(GameState initialState, Players.Player leftP, Players.Player rightP) : base(initialState, leftP, rightP)
        {
        }

        protected override void PerformGameStep()
        {
            Console.WriteLine();
            GameState.Print();
            var currentPlayer = leftPlayerTurn ? leftPlayer : rightPlayer;
            Console.WriteLine($"Waiting for {currentPlayer.Name} move....");

            try
            {
                var move = currentPlayer.GetNextMove(this.GameState);
                if (move.IsValid(GameState))
                    GameState.ApplyMove(move);
                leftPlayerTurn ^= true;
            }
            catch (MoveNotAcceptedException e)
            {
                Console.WriteLine("\n---------------- ERROR ------------------------");
                Console.WriteLine("Move was not accepted. Detected problem:");
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("-----------------------------------------------");
        }

        protected override void PrintEndGameMsg(){
            var winnerName = leftPlayerTurn ? rightPlayer.Name : leftPlayer.Name;
            var looserName = leftPlayerTurn ? leftPlayer.Name : rightPlayer.Name;
            Console.WriteLine($"Player {winnerName} won!");
            Console.WriteLine($"Player {looserName} was defeated.");
        }
    }
}