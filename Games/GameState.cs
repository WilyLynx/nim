using System;
using System.Collections.Generic;
using System.Linq;

namespace nim.Games
{
    public class GameState : ICloneable
    {
        public GameState(List<int> heapsHeights)
        {
            HeapsHeights = heapsHeights;
        }

        public List<int> HeapsHeights { get; private set; }

        public int HeapCount => HeapsHeights.Count;

        public int NumberOfNotEmptyHeaps => HeapsHeights.Where(x => x > 0).Count();

        public void ApplyMove(Move move)
        {
            HeapsHeights[move.HeapNum] -= move.NumberOfElementsToTake;
        }

        public object Clone()
        {
            return new GameState(this.HeapsHeights.ToList());
        }

        public void Print()
        {
            Console.WriteLine("Current game state:");
            Console.WriteLine("Heap num      Heap size");
            for (int i = 0; i < this.HeapsHeights.Count; i++)
                if(this.HeapsHeights[i] > 0) Console.WriteLine($"{i,8}      {this.HeapsHeights[i],9}");
            Console.WriteLine();
        }
    }
}