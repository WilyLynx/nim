using nim.Players;

namespace nim.Games
{
    class GameResult
    {
        public GameResult(int winner,
                          int gameLenght,
                          GameState initialState,
                          Player leftPlayer,
                          Player rightPlayer)
        {
            Winner = winner;
            GameLenght = gameLenght;
            InitialState = initialState;
            LeftPlayer = leftPlayer;
            RightPlayer = rightPlayer;
        }

        public int Winner { get; }
        public int GameLenght { get; }
        public GameState InitialState { get; }
        public Player LeftPlayer { get; }
        public Player RightPlayer { get; }
    }
    
}