using System.Collections.Generic;

namespace nim.Games
{
    public class GameDefinition
    {
        public List<int> HeapSizes { get; }

        public GameDefinition(List<int> heapSizes)
        {
            this.HeapSizes = heapSizes;
        }

        public GameState ToGameState()
        {
            return new GameState(this.HeapSizes);
        }

        public override string ToString()
        {
            return string.Join(" ", HeapSizes);
        }
    }
}