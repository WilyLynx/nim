
using System;
using System.Collections.Generic;
using nim.Players;

namespace nim.Games
{
    abstract class Game
    {
        public GameState GameState { get;}
        public Player leftPlayer { get; }
        public Player rightPlayer { get; }

        protected bool leftPlayerTurn = true;
        public Game(GameState initialState, Player leftP, Player rightP)
        {
            this.GameState = initialState;
            this.leftPlayer = leftP;
            this.rightPlayer = rightP;
        }

        public GameResult Run(){
            var initState = (GameState)this.GameState.Clone();
            int gameLength = 0;
            while(!this.GameHasEnded())
            {
                gameLength++;
                PerformGameStep();
            }
            PrintEndGameMsg();
            int winner = leftPlayerTurn ? 1 : 0;
            return new GameResult(winner, gameLength, initState, leftPlayer, rightPlayer);
        }

        protected abstract void PerformGameStep();
        protected abstract void PrintEndGameMsg();


        private bool GameHasEnded(){
            return GameState.NumberOfNotEmptyHeaps == 0;
        }
    }

}