using System;
using nim.Players;

namespace nim.Games
{
    class TestGame : Game
    {
        public TestGame(GameState initialState, Player leftP, Player rightP) : base(initialState, leftP, rightP)
        {
        }

        protected override void PerformGameStep()
        {
            var currentPlayer = leftPlayerTurn ? leftPlayer : rightPlayer;
            var move = currentPlayer.GetNextMove(this.GameState);
            if (move.IsValid(GameState))
                GameState.ApplyMove(move);
            leftPlayerTurn ^= true;
        }

        protected override void PrintEndGameMsg()
        {
            Console.Write("");
        }
    }
}