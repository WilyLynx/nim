using System;

namespace nim
{
    class ResultTablePrinter
    {
        static int tableWidth = 73;
        public void Print(int[,] resultMatrix, string p1_name, string p2_name)
        {
            PrintLine();
            PrintRow("", p1_name, p2_name);
            PrintLine();
            PrintRow(p1_name, resultMatrix[0,0].ToString(), resultMatrix[0,1].ToString());
            PrintRow(p2_name, resultMatrix[1,0].ToString(), resultMatrix[1,1].ToString());
            PrintLine();
        }

        public static void PrintFormat()
        {
            PrintLine();
            PrintRow("", "Player 1 should win", "Player 2 should win");
            PrintLine();
            PrintRow(" Player 1 won", "", "");
            PrintRow(" Player 2 won", "", "");
            PrintLine();
        }

        static void PrintLine()
        {
            Console.WriteLine(new string('-', tableWidth));
        }

        static void PrintRow(params string[] columns)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCentre(column, width) + "|";
            }

            Console.WriteLine(row);
        }

        static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }
    }
}