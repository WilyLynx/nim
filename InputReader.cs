using System;

namespace nim
{
    public static class InputReader
    {
        public static int ReadNumber(string prompt, int min = 0, int max = int.MaxValue, bool validate_min_max=true)
        {
            int? number = null;
            while (number == null)
            {
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                bool isNumber = int.TryParse(input, out int value);
                if (!isNumber)
                {
                    Console.WriteLine("Please type a correct number");
                }
                else if(validate_min_max && (value < min || value > max))
                {
                    Console.WriteLine($"Please type a number from valid range [{min}, {max}]");
                }
                else
                {
                    number = value;
                }
            }
            return number.Value;
        }
    }
}