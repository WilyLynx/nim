
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nim.Games;

namespace nim.Players
{
    public class BestPlayer : ComputerPlayer
    {
        private readonly Random random;

        public BestPlayer(string name, bool blocking, bool silent = false) : base(name, blocking, silent)
        {
            this.random = new Random();
        }

        protected override Move CalculateNextMove(GameState currentState)
        {
            var position = currentState.HeapsHeights.Aggregate(0,(acc,x)=>acc^x);
            if(position==0){
                var indexes = Enumerable.Range(0, currentState.HeapCount);
                var heights = currentState.HeapsHeights;
                var availableHeaps = Enumerable.Zip(indexes, heights).Where(x => x.Second > 0).ToList();
                var selectedHeap = availableHeaps[random.Next(availableHeaps.Count)];
                var selectedHeapIndex = selectedHeap.First;
                var elementCount = random.Next(1, selectedHeap.Second + 1);
                return new Move(selectedHeapIndex, elementCount);
            }
            else
            {
                var binaryHeights = currentState.HeapsHeights.Select(height =>Convert.ToString(height, 2)).ToList();
                var binaryPosition = Convert.ToString(position, 2);
                var selectedHeap = binaryHeights.FindIndex(height=>height.Length>=binaryPosition.Length && height[height.Length-binaryPosition.Length]=='1');
                var selectedHeapHeightBinary = binaryHeights[selectedHeap];
                var j = 0; 
                var sb = new StringBuilder(selectedHeapHeightBinary);
                for(var i =binaryHeights[selectedHeap].Length-binaryPosition.Length;i<binaryHeights[selectedHeap].Length;i++,j++){
                    if(binaryPosition[j]=='1')
                    {
                        sb[i]=selectedHeapHeightBinary[i]=='1'? '0':'1';
                    }
                }
                var result = Convert.ToInt32(sb.ToString(),2);
                return new Move(selectedHeap,currentState.HeapsHeights[selectedHeap]-result);
            }


        }
    }
}