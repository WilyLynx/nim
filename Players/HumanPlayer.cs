using System;
using nim.Games;

namespace nim.Players
{
    public class HumanPlayer : Player
    {
        public HumanPlayer(string name) : base(name)
        {
        }

        public override Move GetNextMove(GameState currentState)
        {
            var heapNum = InputReader.ReadNumber("Select heap number:", max: currentState.HeapCount - 1, validate_min_max: false);
            var elementCount = InputReader.ReadNumber("Select number of elements:", min: 1, max: currentState.HeapsHeights[heapNum], validate_min_max: false);
            return new Move(heapNum, elementCount);
        }
    }
}