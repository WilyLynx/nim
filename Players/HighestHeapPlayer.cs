using System;
using System.Linq;
using nim.Games;

namespace nim.Players
{
    public class HighestHeapPlayer : ComputerPlayer
    {
        public HighestHeapPlayer(string name, bool blocking, bool silent=false): base(name, blocking, silent) {}

        protected override Move CalculateNextMove(GameState currentState)
        {
            var indexes = Enumerable.Range(0, currentState.HeapCount);
            var heights = currentState.HeapsHeights;
            var availableHeaps = Enumerable.Zip(indexes, heights).Where(x => x.Second > 0).ToList();
            var maxHeight = availableHeaps.Max(x => x.Second);
            var highestHeap = availableHeaps.First(x => x.Second == maxHeight);
            return new Move(highestHeap.First, highestHeap.Second);
        }
    }
}
