using nim.Games;

namespace nim.Players
{
    public abstract class Player
    {
        public Player(string name)
        {
            this.Name = name;
        }

        public string Name { get; }
        public abstract Move GetNextMove(GameState currentState);
    }
}