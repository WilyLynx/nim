using System;
using nim.Games;

namespace nim.Players
{
    public abstract class ComputerPlayer : Player
    {
        private readonly bool blocking;
        private readonly bool silent;

        public ComputerPlayer(string name, bool blocking, bool silent) : base(name)
        {
            this.blocking = blocking;
            this.silent = silent;
        }

        public override Move GetNextMove(GameState currentState)
        {
            var move = CalculateNextMove(currentState);
            if(!silent) Console.WriteLine($"Computer's move: Heap: {move.HeapNum}, ElementCount: {move.NumberOfElementsToTake}");
            if (blocking)
            {
                if(!silent)
                    Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }
            return move;
        }

        protected abstract Move CalculateNextMove(GameState currentState);
    }
}