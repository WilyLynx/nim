using System;
using System.Linq;
using nim.Games;

namespace nim.Players
{
    public class RandomPlayer : ComputerPlayer
    {
        private readonly Random random;
        public RandomPlayer(string name, bool blocking, bool silent=false): base(name, blocking, silent)
        {
            this.random = new Random();
        }

        protected override Move CalculateNextMove(GameState currentState)
        {
            var indexes = Enumerable.Range(0, currentState.HeapCount);
            var heights = currentState.HeapsHeights;
            var availableHeaps = Enumerable.Zip(indexes, heights).Where(x => x.Second > 0).ToList();
            var selectedHeap = availableHeaps[random.Next(availableHeaps.Count)];
            var selectedHeapIndex = selectedHeap.First;
            var elementCount = random.Next(1, selectedHeap.Second + 1);
            return new Move(selectedHeapIndex, elementCount);
        }
    }
}
