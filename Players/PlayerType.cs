namespace nim.Players
{
    public enum PlayerType
    {
        Human,
        Random,
        Optimal,
        RandomHeap,
        HighestHeap,
    }
}

