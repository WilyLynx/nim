using System;
using System.Runtime.Serialization;

namespace nim
{
    [Serializable]
    internal class MoveNotAcceptedException : Exception
    {
        public MoveNotAcceptedException()
        {
        }

        public MoveNotAcceptedException(string message) : base(message)
        {
        }

        public MoveNotAcceptedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MoveNotAcceptedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}