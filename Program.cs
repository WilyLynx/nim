﻿using System;
using System.Collections.Generic;
using nim.Games;
using nim.Players;

namespace nim
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0){
                if(args[0] == "tests")
                TestMode();
            }
            else
                GameMode();

            Console.WriteLine("Press key to exis.");
            Console.ReadLine();
        }

        private static void GameMode()
        {
            PrintWelcomeMsg();
            var predefinedGames = GetMockGame();
            var mainMenu = new MainMenu(predefinedGames);
            mainMenu.Run();
        }

        private static void TestMode()
        {
            Console.WriteLine("Welcome to NIM test mode\nPress Ctrl+C to abort tests.\n");
            Console.WriteLine("Tests results format:");
            ResultTablePrinter.PrintFormat();
            Console.WriteLine();
            Console.WriteLine("Test results:");
            var players = new List<ComputerPlayer>{
                new HighestHeapPlayer("HighestHeap", false, true),
                new RandomHeapPlayer("RandomHeap", false, true),
                new RandomPlayer("Random", false, true),
                new BestPlayer("Optimal", false, true),
                };
            var tester = new StrategyTester(players, numberOfGamesInEachPair: 1000);
            tester.Run();
        }

        private static void PrintWelcomeMsg()
        {
            Console.WriteLine("Welcome to NIM Game\nPress Ctrl+C to exit.\n");
        }

        private static List<GameDefinition> GetMockGame()
        {
            return new List<GameDefinition>
            {
                new GameDefinition(new List<int> { 1, 4, 4 }),
                new GameDefinition(new List<int> { 1, 2, 3, 4, 5 }),
                new GameDefinition(new List<int> { 6, 6, 6, 6 }),
            };
        }
    }
}
