# NIM

Implementacja gry NIM z wykorzystaniem .NET Core.

Autorzy:

- Łukasz Kiełbus
- Michał Możdżonek
- Łukasz Osowicki
  
## Uruchamianie

Tryb gry:

```{console}
    cd nim
    dotnet run
```

Tryb testów:

```{console}
    cd nim
    dotnet run tests
```
