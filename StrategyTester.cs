using System;
using System.Collections.Generic;
using nim.Games;
using nim.Players;

namespace nim
{
    class StrategyTester
    {
        private static int minHeapsCount = 4;
        private static int maxHeapsCount = 16;
        private static int minHeapSize = 32;
        private static int maxHeapSize = 1_048_576;  // 2^20
        private Random rng = new Random();
        private ResultTablePrinter tablePrinter = new ResultTablePrinter();
        private readonly List<ComputerPlayer> players;
        private readonly int numberOfGamesInEachPair;

        public StrategyTester(List<ComputerPlayer> players, int numberOfGamesInEachPair)
        {
            this.players = players;
            this.numberOfGamesInEachPair = numberOfGamesInEachPair;
        }

        public void Run()
        {
            for(int ip1=0; ip1 < players.Count; ip1++)
            {
                for(int ip2=0; ip2 < ip1; ip2++)
                {
                    TestPair(ip1, ip2);
                    TestPair(ip2, ip1);
                }
                TestPair(ip1, ip1);
            }
        }

        private void TestPair(int ip1, int ip2)
        {
            ComputerPlayer p1 = players[ip1];
            ComputerPlayer p2 = players[ip2];
            List<GameResult> results = PerformTest(p1, p2);
            int[,] resultMatrix = ConvertResultsToMatrix(results);
            tablePrinter.Print(resultMatrix, p1.Name, p2.Name);
        }

        private List<GameResult> PerformTest(ComputerPlayer p1, ComputerPlayer p2)
        {
            List<GameResult> results = new List<GameResult>();
            for (int i = 0; i < this.numberOfGamesInEachPair; i++)
                results.Add(generateTestGame(p1, p2).Run());
            return results;
        }

        private int[,] ConvertResultsToMatrix(List<GameResult> results)
        {
            int[,] resultMatrix = new int[2, 2];
            foreach (var r in results)
            {
                bool leftShouldWin = analyzeInitialGameState(r.InitialState);
                int shouldWinIdx = leftShouldWin ? 0 : 1;
                resultMatrix[r.Winner, shouldWinIdx]++;
            }

            return resultMatrix;
        }

        private bool analyzeInitialGameState(GameState initialState)
        {
            return getNimSum(initialState.HeapsHeights) != 0;
        }

        private int getNimSum(List<int> nums)
        {
            int w = 0;
            foreach (var s in nums)
            {
                w ^= s;
            }
            return w;
        }

        private TestGame generateTestGame(ComputerPlayer p1, ComputerPlayer p2)
        {
            int heapsCount = rng.Next(minHeapsCount, maxHeapsCount);
            List<int> heapSizes = new List<int>();
            for (int i = 0; i < heapsCount-1; i++)
            {
                heapSizes.Add(rng.Next(minHeapSize, maxHeapSize));
            }
            int nimSum = getNimSum(heapSizes);
            // 50% chances for generating P position
            if(rng.NextDouble() > 0.5)
                heapSizes.Add(nimSum);
            else
            {
                int h = rng.Next(minHeapSize, maxHeapSize);
                while(h == nimSum) h = rng.Next(minHeapSize, maxHeapSize);
                heapSizes.Add(h);
            }
                
            var gs = new GameState(heapSizes);
            return new TestGame(gs, p1, p2);
        }
    }
}