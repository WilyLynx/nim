import os
import re

os.chdir('results_parsing')
RAW_TABLE_ROWS_COUNT = 6
with open('results_raw.txt', 'r') as f:
    data = f.readlines()

with open('results_latex.tex', 'w') as f:
    table_count = len(data) // RAW_TABLE_ROWS_COUNT
    for i in range(table_count):
        start_idx = i * RAW_TABLE_ROWS_COUNT
        data_row_0 = re.sub(' +', ' ', data[start_idx + 3]).split(' ')
        data_row_1 = re.sub(' +', ' ', data[start_idx + 4]).split(' ')
        player_0 = data_row_0[1]
        player_1 = data_row_1[1]
        values = [
            [data_row_0[3], data_row_0[5]],
            [data_row_1[3], data_row_1[5]],
        ]
        f.write('\\begin{table}[htp]\n')
        f.write('\centering\n')
        f.write('\\begin{tabular}{|c|c|c|}\n')
        f.write('\\hline\n')
        f.write(f' &  {player_0} &  {player_1}\\\\\n')
        f.write('\\hline\n')
        f.write(f'{player_0}  & {values[0][0]}  & {values[0][1]}\\\\\n')     
        f.write(f'{player_1}  & {values[1][0]}  & {values[1][1]}\\\\\n')
        f.write('\\hline\n')
        f.write('\\end{tabular}\n')
        f.write('\\caption{'+player_0+' vs '+player_1+'}\n')
        f.write('\\label{tab:res_'+str(i)+'}\n')
        f.write('\\end{table}\n')
        f.write('\n')
